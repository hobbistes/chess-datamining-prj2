// I had to comment out this setting:
// #dbms.directories.import=import

// Constraints
CREATE CONSTRAINT ON (p:Player) ASSERT p.name IS UNIQUE;
CREATE CONSTRAINT ON (e:Event) ASSERT e.name IS UNIQUE;
CREATE CONSTRAINT ON (g:Game) ASSERT g.id IS UNIQUE;
CREATE CONSTRAINT ON (f:Fen) ASSERT f.fen IS UNIQUE;

// Load nodes from game_info
// Takes ~0.3 seconds after fixes
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS
FROM "file:/home/gchoumo/cur/game_info.csv" AS row
MERGE (player1:Player { name: row.Black })
MERGE (player2:Player { name: row.White })
MERGE (event:Event { name: row.Event, date: row.EventDate, site: row.Site })
MERGE (game:Game {gamenumber: row.GameNumber, date: row.Date, moves: row.Moves,
					hmoves: row.HalfMoves, opening: row.Opening, eco: row.ECO,
					welo: row.WhiteElo, belo: row.BlackElo, result: row.Result,
					id: row.id})

// Load nodes from Moves
// Takes ~2.7 seconds after the fixes
LOAD CSV WITH HEADERS FROM "file:/home/gchoumo/cur/moves.csv" AS row
MERGE (fen:Fen { fen: row.FEN })

// Game-Players Relationship - First load info
// Takes ~0.3 seconds after the fixes
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS
FROM "file:/home/gchoumo/cur/game_info.csv" AS row
MERGE (p1:Player {name: row.White})
MERGE (p2:Player {name: row.Black})
MERGE (g:Game {id: row.id})
MERGE (p1)-[r1:Plays { Color: 'White'}]->(g)
MERGE (p2)-[r2:Plays { Color: 'Black'}]->(g)

// Game - Event Relationship
// Takes 0.2 seconds after the fixes
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS
FROM "file:/home/gchoumo/cur/game_info.csv" AS row
MERGE (ev:Event {name: row.Event})
MERGE (g:Game {id: row.id})
MERGE (ev)-[r3:HasGame {round: row.Round}]->(g)

// Game - FEN Relationship through a move
// Needs ~5.5 seconds
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS
FROM "file:/home/gchoumo/cur/moves.csv" AS row
MERGE (g:Game {id: row.GameNumber})
MERGE (f:Fen {fen: row.FEN})
MERGE (g)-[r4:HasFen {
						move: row.Move,
						movenumber: row.MoveNumber,
						side: row.Side }]->(f)
