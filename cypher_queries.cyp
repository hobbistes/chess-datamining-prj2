// Query 1.
MATCH (g:Game)-[:HasFen]->(f:Fen {
	fen: 'r1bqkbnrpppp1ppp2n51B2p34P35N2PPPP1PPPRNBQK2R'
	})
WITH count(*) AS gwf
MATCH (g:Game {result: 'White'})-[:HasFen]->(f:Fen {
	fen: 'r1bqkbnrpppp1ppp2n51B2p34P35N2PPPP1PPPRNBQK2R'
	})
WITH count(*) AS gwf_white, gwf
RETURN "Number of games with the FEN: ", gwf,
	"Percentage of those that White wins: ", (100*gwf_white)/(1.0*gwf)


// Query 2.
MATCH (g:Game)-[:HasFen]->(f:Fen {
	fen: 'r1bqkbnrpppp1ppp2n51B2p34P35N2PPPP1PPPRNBQK2R'
	})
WITH count(*) as gwf
MATCH (g:Game {result: 'White'})-[:HasFen]->(f:Fen {
	fen: 'r1bqkbnrpppp1ppp2n51B2p34P35N2PPPP1PPPRNBQK2R'
	})
WITH count(*) as gwf_white, gwf
MATCH (g:Game {result: 'Black'})-[:HasFen]->(f:Fen {
	fen: 'r1bqkbnrpppp1ppp2n51B2p34P35N2PPPP1PPPRNBQK2R'
	})
WITH count(*) as gwf_black,gwf_white, gwf
MATCH (g:Game {result: 'Draw'})-[:HasFen]->(f:Fen {
	fen: 'r1bqkbnrpppp1ppp2n51B2p34P35N2PPPP1PPPRNBQK2R'
	})
WITH count(*) as gwf_draw,gwf_black,gwf_white,gwf
RETURN "Number of games with that FEN:", gwf,
  "White won: ", gwf_white,
  "Black won: ", gwf_black,
  "Draws: ", gwf_draw


// Query 3.
MATCH (ev:Event), (g:Game),
  (ev)-[r:HasGame]->(g)
WITH ev.name AS ev_name, count(*) AS max_games
ORDER BY max_games DESC, ev_name DESC
LIMIT 1
MATCH (p:Player {name: 'Karpov  Anatoly'})
MATCH (ev:Event {name: ev_name})
MATCH (ev)-[r1:HasGame]->(g:Game)
MATCH (p)-[r2:Plays]->(g)
WITH count(*) as games_played, ev_name
MATCH (p:Player {name: 'Karpov  Anatoly'})
MATCH (ev:Event {name: ev_name})
MATCH (ev)-[r1:HasGame]->(g:Game)
MATCH (p)-[r3:Plays {Color: 'White'}]->(g)
WITH count(*) as gwh, games_played, ev_name
MATCH (p:Player {name: 'Karpov  Anatoly'})
MATCH (ev:Event {name: ev_name})
MATCH (ev)-[r1:HasGame]->(g:Game)
MATCH (p)-[r4:Plays {Color: 'Black'}]->(g)
WITH count(*) as gb, gwh, games_played, ev_name
RETURN "Games he played on that tournament: ", games_played,
  "Games he played being White: ", gwh,
  "Games he played being Black: ", gb


// Query 4.
MATCH (p:Player)
MATCH (g:Game {opening: 'Ruy Lopez'})
MATCH (p)-[:Plays]->(g)
WITH p.name as p_name, count(*) as num_games
ORDER BY num_games DESC
RETURN "Most games opening with Ruy Lopez. Name of player: ", p_name,
  "Number of such games: ", num_games
LIMIT 1


// Query 5.
MATCH (p:Player)
MATCH (g:Game)
MATCH (f1:Fen), (f2:Fen), (f3:Fen)
MATCH (p)-[r1:Plays]->(g)
MATCH (g)-[r2:HasFen {move: 'Nc6'}]->(f1)
MATCH (g)-[r3:HasFen {move: 'Bb5'}]->(f2)
MATCH (g)-[r4:HasFen {move: 'a6'}]->(f3)
WHERE TOINT(r3.movenumber) = TOINT(r2.movenumber)+1
AND TOINT(r4.movenumber) = TOINT(r3.movenumber)+1
WITH distinct p.name as p_name, count(*) as num
RETURN "Player ", p_name, " - Number of games: ", num


// Query 6.
MATCH (g:Game {id: '636'})
MATCH (ev:Event)
MATCH (p:Player)
MATCH (ev)-[r1:HasGame]->(g)
MATCH (p)-[:Plays]-(g)
RETURN "Game with id 636 info: ", g,
  "Tournament: ", ev.name,
  "Players: ", collect(p.name)

MATCH (g:Game {id: '636'})
MATCH (ev:Event)
MATCH (ev)-[r1:HasGame]->(g)
MATCH (f:Fen)
MATCH (g)-[r2:HasFen]->(f)
WITH r2.move as f_move, r2.movenumber as m_num
RETURN "Move number: ", m_num, "Move: ", f_move
ORDER BY TOINT(m_num) ASC


// Query 7.
MATCH (g:Game)-[r1:HasFen]->(f1:Fen {
	fen: 'r1bqkbnrpppp1ppp2n51B2p34P35N2PPPP1PPPRNBQK2R'
	})
MATCH (g)-[r2:HasFen {movenumber: toString(toInteger(r1.movenumber)+1)}]->(f2:Fen)
MATCH (p:Player)-[r3:Plays {Color: g.result}]->(g)
WHERE r2.move <> 'a6'
RETURN "Game:", g.id,
  "Move num with fen:", r1.movenumber,
  "Next move (not a6):", r2.move,
  "Winner:", g.result,
  "Name:", p.name
