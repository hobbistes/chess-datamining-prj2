import re
import csv

with open("chessData.txt") as f:
	contents = f.readlines()

for i in range(len(contents)):
	contents[i] = contents[i].strip()

# We also add an id column
game_headers = [
	"id",
	"White",
	"Black",
	"Date",
	"HalfMoves",
	"Moves",
	"Result",
	"WhiteElo",
	"BlackElo",
	"GameNumber",
	"Event",
	"Site",
	"EventDate",
	"Round",
	"ECO",
	"Opening"
]

move_headers = [
	"MoveNumber",
	"Side",
	"Move",
	"FEN",
	"GameNumber"
]

gamelist = [game_headers]
movelist = [move_headers]

# Patterns
game_moves = re.compile("^-+ Game Moves -+$")
moves_end  = re.compile("^=+$")

i = 0
game_id = 1
while i < len(contents):
	cur_game  = [str(game_id)]
	cur_moves = []

	# Dump the first line
	i+=1

	# Get the game info
	while not game_moves.match(contents[i]):
		cur_game.append(contents[i].split(' ',1)[1])
		i+=1

	# Dump the moves header
	i+=1

	# And now get the moves
	while not moves_end.match(contents[i]):
		this_move = []
		split1 =  contents[i].split(',')
		for item in split1:
			this_move.append(item.split(':',1)[1].strip())
		cur_moves.append(this_move)
		i+=1

	# Dump the moves end line
	i+=1

	# Increment the game_id
	game_id += 1

	# append/extend to the main lists
	gamelist.append(cur_game)
	movelist.extend(cur_moves)

print("Game list length is {0}".format(len(gamelist)))
print("Move list length is {0}".format(len(movelist)))

# Write to the game_info.csv
with open("game_info.csv", "w") as f:
	writer = csv.writer(f)
	writer.writerows(gamelist)

# Write to the moves.csv
with open("moves.csv", "w") as f:
	writer = csv.writer(f)
	writer.writerows(movelist)

print("Done")